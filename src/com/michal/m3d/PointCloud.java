/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.michal.m3d;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.GLES20;

import com.michal.util.Config;

/**
 * A two-dimensional plane for use as a drawn object in OpenGL ES 2.0.
 */
public class PointCloud {

	private final String vertexShaderCode =
	// This matrix member variable provides a hook to manipulate
	// the coordinates of the objects that use this vertex shader
	"uniform mat4 uMVPMatrix;"
			+ "attribute vec4 vPosition;"
			+ "void main() {"
			+
			// The matrix must be included as a modifier of gl_Position.
			// Note that the uMVPMatrix factor *must be first* in order
			// for the matrix multiplication product to be correct.
			"  gl_Position = uMVPMatrix * vPosition;" + "gl_PointSize = 2.0; "
			+ "}";


	private final String fragmentShaderCode = "precision mediump float;"
			+ "uniform vec4 vColor;" + "void main() {"
			+ "  gl_FragColor = vColor;" + "}";
	private final int MaxPointCout = Config.GLESVertexBufferSize; //1 000 000;
	private final FloatBuffer vertexBuffer;
	private final int mProgram;
	private int mPositionHandle;
	private int mColorHandle;
	private int mMVPMatrixHandle;
	private int pointCount =0;
	//private int lastBuffPosition =0;
	// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;



	private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per
															// vertex

	float color[] = { 0.0f, 1.0f, 0.0f, 1.0f };

	/**
	 * Sets up the drawing object data for use in an OpenGL ES context.
	 */
	public PointCloud() {
		
		// initialize vertex byte buffer for shape coordinates
		//allocate data buffer
		//1 point = 3 floats = 3*4 bytes
		//max  point let's say one million 1000000
		ByteBuffer bb = ByteBuffer.allocateDirect(
		
				MaxPointCout*4);
		bb.order(ByteOrder.nativeOrder());
		vertexBuffer = bb.asFloatBuffer();
		
		vertexBuffer.position(0);


		// prepare shaders and OpenGL program
		int vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCode);
		int fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
				fragmentShaderCode);

		mProgram = GLES20.glCreateProgram(); // create empty OpenGL Program
		GLES20.glAttachShader(mProgram, vertexShader); // add the vertex shader
														// to program
		GLES20.glAttachShader(mProgram, fragmentShader); // add the fragment
															// shader to program
		GLES20.glLinkProgram(mProgram); // create OpenGL program executables

		
		vertexBuffer.position(0);
	}
	synchronized public void clearPoints()
	{
		pointCount =0;
		vertexBuffer.clear();
		vertexBuffer.position(0);
	}
	
	synchronized public boolean addPoint(float x, float y, float z)
	{
		
		if (pointCount*3+3>vertexBuffer.limit()) return false;
		vertexBuffer.position(pointCount*3);
		vertexBuffer.put(x);
		vertexBuffer.put(y);
		vertexBuffer.put(z);
		pointCount++;
		vertexBuffer.position(0);
		return true;
		
	}
	/**
	 * Encapsulates the OpenGL ES instructions for drawing this shape.
	 * 
	 * @param mvpMatrix
	 *            - The Model View Project matrix in which to draw this shape.
	 */
	synchronized public void draw(float[] mvpMatrix) {
		
		// Add program to OpenGL environment
		GLES20.glUseProgram(mProgram);

		// get handle to vertex shader's vPosition member
		mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

		// Enable a handle to the triangle vertices
		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Prepare the triangle coordinate data
		GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);

		// get handle to fragment shader's vColor member
		mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

		// Set color for drawing the triangle
		GLES20.glUniform4fv(mColorHandle, 1, color, 0);

		// get handle to shape's transformation matrix
		mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
		MyGLRenderer.checkGlError("glGetUniformLocation");

		// Apply the projection and view transformation
		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
		MyGLRenderer.checkGlError("glUniformMatrix4fv");

		// Draw the square
		GLES20.glDrawArrays(GLES20.GL_POINTS, 0, pointCount);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandle);
	}

}