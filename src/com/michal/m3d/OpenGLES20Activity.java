/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.michal.m3d;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Queue;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.michal.util.Config;
import com.michal.util.MyApp;



public class OpenGLES20Activity extends Activity {
	Context context;
	//inner class
//	class DataBaseWriter implements Runnable
//	{
//		public SQLiteDatabase db;
//		public DataBaseWriter(SQLiteDatabase _db) {
//			db = _db;
//		}
//		@Override
//		public void run() {
//			Log.d("m3d", "DATABASE writer started");
//			
//			Queue<point3D> points = mGLView.getPointCloudBuilder().getPointQueue();
//			
//			Log.d("m3d", "SQL Transaction BEGIN");
//			ContentValues values = new ContentValues();
//			values.put("some", "0");
//			sessionID = db.insert("sessions", null, values);
//			Log.d("m3d", "Session ID: "+Long.toString(sessionID));
//			if (sessionID == -1)
//			{
//				Log.w("m3d", "CANNOT ADD SESSION METADATA!!");
//			}
//			db.beginTransaction();
//			 while(points.size()<1000) 
//			 {
//				 try {
//					Thread.sleep(20);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			 }
//				Log.d("m3d", "Start adding points to DB");
//				Log.d("m3d", "=========================");
//		        while(!mGLView.getPointCloudBuilder().isDone() || points.size()>10)
//		        {
//		        	 
//		        	point3D p = points.poll();
//		        	if(p!= null )
//		        	{
//		        	db.execSQL("INSERT INTO points (sessionID ,X,Y,Z) VALUES ("+Integer.toString((int)sessionID)+","+ Float.toString(p.x)+" , "+ Float.toString(p.y) +"," + Float.toString(p.z) + ")");
//		        	}
//		        	
//		        	
//		        	else
//		        	{
//		        		 try {
//		 					Thread.sleep(20);
//		 				} catch (InterruptedException e) {
//		 					// TODO Auto-generated catch block
//		 					e.printStackTrace();
//		 				}
//		        	}
//		        	
//		        }
//		    db.setTransactionSuccessful();    
//			db.endTransaction();
//			//db.close();
//			Log.d("m3d", "SQL Transaction OVER");
//			Log.d("m3d", "DATABASE writer ended");
//			showToast("Done!");
//
//		}
//		
//	}
//	
	
//	private Thread tDatabeWriter = null;
	private long sessionID =-1;
	//inner class
	
	private class BackgroundTask extends AsyncTask<String, Integer, String> {
		private ProgressDialog dialog;
		OpenGLES20Activity activity;
		public BackgroundTask(OpenGLES20Activity activity) {
			this.activity = activity;
		}

		@Override
		protected String doInBackground(String... arg0) {
			
		        activity.savePointCloud();
		 
			return null;
		}
	   @Override
	    protected void onPreExecute() {
		      SaveButton.setEnabled(false);
	    }
		     
		@Override
		protected void onPostExecute(String result) {
			SaveButton.setEnabled(true);
	}
	}

	private MyGLSurfaceView mGLView;
	private Button SaveButton;
	private Button MakeButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		context = getApplicationContext();
		
		// Create a GLSurfaceView instance and set it
		// as the ContentView for this Activity
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gles20activity);

		mGLView = (MyGLSurfaceView) (findViewById(R.id.myGLSurfaceView1));
		SaveButton = (Button) findViewById(R.id.buttonSave);
		MakeButton = (Button) findViewById(R.id.buttonMake);

		// setContentView(mGLView);
		// addContentView (mGLView, new ViewGroup.LayoutParams
		// (ViewGroup.LayoutParams.WRAP_CONTENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT));
		// Layout l = (Layout)findViewById(R.layout.activity_main);

		// addContentView (mGLView, new ViewGroup.LayoutParams
		// (ViewGroup.LayoutParams.MATCH_PARENT,
		// ViewGroup.LayoutParams.FILL_PARENT));
	}

	public void onSaveButton(View v) {

		BackgroundTask task = new BackgroundTask(this);
		task.execute();
		
	}

	public void savePointCloud()
	{
		 Queue<point3D> points = mGLView.getPointCloudBuilder().getPointQueue();
		 
		/*save to database*/
		
		long sessionID = MyApp.getInstance().RegisterSession();
		SQLiteDatabase db = MyApp.getInstance().getDatabase();
		showToast( "Point cloud with ID "+Long.toString(sessionID) +" is being saved. \nPoints to save:"+Integer.toString(points.size()/1000)+"  thousands");
		long beg = System.currentTimeMillis();
		Log.d("m3d", "Point cloud with ID "+Long.toString(sessionID) +" is being saved. \nPoints to save:"+Integer.toString(points.size()/1000)+"  thousands");
		
		if (Config.saveMode == Config.SAVE_MODE_BINARY)
		{
			try {	
				File dest = new File (Config.DatabaseDir+"pc"+Long.toString(sessionID)+".dat");
				FileOutputStream out = new FileOutputStream(dest);
				Iterator<point3D> iterator = points.iterator();
				int count =0;
				while(iterator.hasNext())
		        {
					count++;
					point3D p = iterator.next();
					ByteBuffer b = ByteBuffer.allocateDirect(4*4);
				
					b.putInt(count);
					b.putInt((int) (1000*p.x));
					b.putInt((int) (1000*p.y));
					b.putInt((int) (1000*p.z));
					out.write(b.array());
		        }
				out.flush();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		if (Config.saveMode == Config.SAVE_MODE_SQLLITE)
		{
		db.beginTransaction();
	        Iterator<point3D> iterator = points.iterator();
	        int count =0;
	        while(iterator.hasNext())
	        {
	        	count++;
        	point3D p = iterator.next();
	        	db.execSQL("INSERT INTO points (sessionID,X,Y,Z) VALUES ("+Integer.toString((int)sessionID)+","+ Float.toString(p.x)+" , "+ Float.toString(p.y) +"," + Float.toString(p.z) + ")");
	        	
	        }
		
	    db.setTransactionSuccessful();    
		db.endTransaction();
		}
		else
			if (Config.saveMode == Config.SAVE_MODE_NOSAVE)
			{
			
		        Iterator<point3D> iterator = points.iterator();
		        int count =0;
		        while(iterator.hasNext())
		        {
		        	count++;
	        	point3D p = iterator.next();
		             	
		        }
			
			}	
		
		int elapsed = (int) (System.currentTimeMillis()-beg);
		
		showToast( "Point cloud has been saved. Took "+Integer.toString(elapsed));
		Log.d("m3d", "Point cloud has been saved. Took "+Integer.toString(elapsed));
//		
//		Log.d("m3d", "SQL Transaction OVER");
//		
		/* saving to file*/
	/*	String sdCard = System.getenv("EXTERNAL_STORAGE");
		File dir = new File("/storage/sdcard0/");
		String date = Calendar.getInstance().getTime().toString();
		FileWriter file;
		try {
			file = new FileWriter(dir.getAbsoluteFile()+"/measurment"+date+".asc");
			BufferedWriter f = new BufferedWriter(file);
			mGLView.getPointCloudBuilder().saveToFile(f);
			f.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//Log.d("m3d", "Saving FILE : " + file.getAbsolutePath());
		*/
	}
	public void onMakeScan(View v) {
		tToast("Wait for scan done");
		//mGLView.getPointCloudBuilder().setDatabase(dbHelper.getWritableDatabase());
		mGLView.startScan();
		//tDatabeWriter = new Thread (new DataBaseWriter(MyApp.getInstance().getDatabase()));
		
		//if (com.michal.util.Config.allowDbLoadDuringScan) tDatabeWriter.start();
		//tDatabeWriter.setPriority(Thread.MIN_PRIORITY);
		
		}

	public void onClear(View v) {
		tToast("Clearing");
		mGLView.getPointCloudBuilder().setDone();
		mGLView.clear();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
		// The following call pauses the rendering thread.
		// If your OpenGL application is memory intensive,
		// you should consider de-allocating objects that
		// consume significant memory here.
		mGLView.getPointCloudBuilder().setDone();
		mGLView.clear();
		mGLView.onPause();
		
	}

	private void tToast(String s) {
		
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, s, duration);
		toast.show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// The following call resumes a paused rendering thread.
		// If you de-allocated graphic objects for onPause()
		// this is a good place to re-allocate them.
		// mGLView.onResume();
	}
	public void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	            Toast.makeText(OpenGLES20Activity.this, toast, Toast.LENGTH_LONG).show();
	        }
	    });
	}
}