package com.michal.m3d;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import android.annotation.SuppressLint;
import android.util.Log;

import com.michal.LMS.Scan;
import com.michal.LMS.laserThread;
import com.michal.LMS.perleThread;
import com.michal.util.Config;



@SuppressLint("NewApi")
public class PointCloudBuilder implements Runnable {

	MyGLRenderer mgl;
	private Queue<point3D> points;
	private boolean done =false;
	//private SQLiteDatabase db = null;
	
	public void setDone()
	{
		done = true;
		
	}
	public boolean isDone()
	{
		return done;
	}
//	public void setDatabase (SQLiteDatabase db)
//	{
//		this.db = db;
//	}
	
	public PointCloudBuilder(final MyGLRenderer _mgl) {
		mgl = _mgl;
		points = new LinkedList<point3D>();
	}
	
	public Queue<point3D> getPointQueue()
	{
		return points;
	}
	
	
	public boolean saveToFile (BufferedWriter outputStreamWriter)
	{
		 try {
		        Iterator<point3D> iterator = points.iterator();
		        while(iterator.hasNext())
		        {
		        	point3D p = iterator.next();
		        	String data= Float.toString(p.x) +"\t"+Float.toString(p.y)+"\t"+Float.toString(p.z)+"\n";
		        	outputStreamWriter.write(data);
		        }
		        outputStreamWriter.close();
		    }
		    catch (IOException e) {
		        Log.e("Exception", "File write failed: " + e.toString());
		    } 
		return false;
	}
	
	@SuppressLint("NewApi")
	@Override
	public void run() {
		
		done = false;
		points.clear();
		// start laser stread
		final laserThread lt = new laserThread();
		
		lt.setIp(Config.IProtatingLaser);
		Thread tlt = new Thread(lt);
		tlt.setPriority(Thread.MAX_PRIORITY);
		tlt.start();
		final perleThread pt = new perleThread(lt);
		
		Thread tpt = new Thread(pt);
		tpt.setPriority(Thread.MAX_PRIORITY);
		tpt.start();
		//p�tla w�tku dzia�a do poki w�tek
		//perle thread si� nie zako�czy i nie ma skan�w do skojarzenia
		
		while (!done) {
			
			PointCloud mPc;
			mPc = mgl.getPointCloud();
			Scan scan = null;
			//Scan scans[] = null;
			
			if (lt.Scans.size() > 20) {

				scan = lt.Scans.pollFirst();

				// if (mPc != null && scan != null && scan.encPos != -1) {
				// Log.w("m3d", "scan with unassigned enc id : " +
				// Integer.toString(Scan.id));
				// }
				float encPos = pt.getPosition(scan.timeStamp);
				//Log.w("m3d", "scan enc pos. " + encPos);
				done = pt.isMoreThanHalfRev(encPos);
				if (mPc != null && scan != null && encPos != -1.0f) {

					// Log.w("m3d", "adding scan to scene : " +
					// Integer.toString((int)scan.timeStamp));
					// mPc.clearPoints();
					final int size = scan.Dist1.size();

					
					for (int i = 0; i < size; i = i + 1) {

						final float ang = (float) (-0.75 * Math.PI + i * 1.5
								* Math.PI / size);
						final float od = Config.od;
						final float aLms = (float) scan.Dist1.get(i);
						final float cosLms = (float) Math.cos(ang);
						final float sinLms = (float) Math.sin(ang);
						final float cosEn = (float) Math.cos(encPos);
						final float sinEn = (float) Math.sin(encPos);
						
						final float x = sinEn * aLms * sinLms + od * cosEn;
						final float y = cosEn * aLms * sinLms + od * sinEn;
						final float z = -aLms * cosLms;
						// float x = (float) ((a*Math.cos(ang)*Math.sin(aE))
						// );
						// float y = (float)
						// (0.0005*(scan.Dist1.get(i)*Math.cos(ang)*Math.cos(aE)));
						// float z = (float)
						// (0.0005*(scan.Dist1.get(i)*Math.sin(ang)));
						
						points.add(new point3D(x, y, z));
						mPc.addPoint(0.0005f * x, 0.0005f * y, 0.0005f * z);
						
						
					}
					
				}
				
			}
			
			// Log.d("m3d",Float.toString(aE));
			
			// od czasu do czasu �adujemy do bazy
			/*
			if (points.size()>500 && db != null)
			{
				db.beginTransaction();
				while(true)
				{
					point3D p = points.poll();
					if (p != null)
					{
						db.execSQL("INSERT INTO points (X,Y,Z) VALUES ("+ Float.toString(p.x)+" , "+ Float.toString(p.y) +"," + Float.toString(p.z) + ")");
					}
					else
					{
						break;
					}
				}
				db.endTransaction();

			}
			*/
			try {
				Thread.sleep(15);
			} catch (final InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		pt.setDone();
		
		Log.d("m3d", "end thread poincloud builder");
	}

}