package com.michal.LMS;

import java.util.Vector;

public class Scan
{
	static public int  id =0;
	public Scan ()
	{
		timeStamp = System.currentTimeMillis();
		encPos = -1;
		id ++;
	}
	public Vector<Integer> Dist1 = new Vector<Integer>(541);
	//public Vector<Integer> RSSI1 = new Vector<Integer>(541);
	public float encPos;
	public long timeStamp ;
	public static float sin [];
	public static float cos [];

	static void buildLookUpTable(int num)
	{
		sin = new float [num];
		cos = new float [num];
		
		for (int i=0; i < num;i++)
		{
			double ang=  (-0.75*Math.PI+i*1.5*Math.PI/num) ;
			sin[i] = (float) Math.sin(ang);
			cos[i] = (float) Math.cos(ang);
			
		}
	}
	
}
