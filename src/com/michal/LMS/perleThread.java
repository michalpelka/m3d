package com.michal.LMS;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import android.annotation.SuppressLint;
import android.util.Log;
import com.michal.util.Config;
class measurment
{
	float aE;
	float speed;
	long time;
}
@SuppressLint("NewApi")
public
class perleThread implements Runnable {
	ConcurrentLinkedQueue<measurment> measuments = new ConcurrentLinkedQueue<measurment>();
	BufferedReader in;
	BufferedWriter out;
	private static float MAXSPEED =1.2f;
	private static int MAXQUE =100000;
	
	private Socket s;
	private final laserThread myLaser;
	private final char request[]= {0x04, '1', '1', ':','1', 0x05,0x00};
	private boolean done = false;
	private Float startAng = null;
	float aE;
	private float lastaE;
	private long lastTime;	
	private int deltaTime;
	float speed;
	
	public boolean isMoreThanHalfRev(double ang)
	{
		if (startAng > Math.PI  && ang < Math.PI && ang > -Math.PI + startAng) return true;
		if (startAng < Math.PI  && ang > Math.PI && ang > Math.PI + startAng) return true;
		return false;
	}
	public void setDone()
	{
		done = true;
	}
	public boolean isDone()
	{
		return done;
	}
	//get speed in rad/sec
	public float getRotSpeed()
	{
		return speed;
	}
	
	public float getPosition(long time)
	{
		if (measuments.size()<10) return -1.0f;
		//int closest =-1;
		measurment mCloset = measuments.peek();
		if (mCloset == null) return -1.0f;
		long closestTime =10000;
		Iterator<measurment> it = measuments.iterator();
		while (it.hasNext())
		{
			measurment m = it.next();
			if (Math.abs(m.time - time)<closestTime && Math.abs(m.speed)< MAXSPEED)
			{
				mCloset = m;
				closestTime=Math.abs(m.time - time);
			}
		}
		float offset = mCloset.speed*closestTime*0.001f;
		return mCloset.aE+offset;
	}
	
	private int findChar (final char ch, final char [] buffer)
	{
		for (int i=0; i < buffer.length; i++)
		{
			if (buffer[i] == ch) return i;
		}
		return -1;
	}
	
	public perleThread(final laserThread _mLaserThread)
	{
		myLaser  = _mLaserThread;
	}
	@SuppressLint("NewApi")
	public void run() {
		done = false;
		measuments.clear();
		Log.d("m3d", "Perle thread started");
		try {
			s = new Socket(Config.IPencoder, 10001);
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new BufferedWriter(
					new OutputStreamWriter(s.getOutputStream()));
			
			
			
		} catch (final UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (final IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lastTime = System.currentTimeMillis();
		while (!done)
		{
			try {
				out.write(request);
				out.flush();
				Thread.sleep(3);
				if (s.getInputStream().available()>4)
				{
					//to be sure that we got all data
					Thread.sleep(4);
				
					final char buf[] = new char[100];
					in.read(buf);
					final int  spaceInd = findChar((char)0x02, buf)+3;
					final int CrInd = findChar((char)0x03, buf);
					if (spaceInd != -1 && CrInd != -1 && spaceInd<CrInd)
					{
						final String tel = new String (buf).substring(spaceInd, CrInd);
					
						lastaE = aE;
						aE = (float) (1.0*Math.abs(Integer.parseInt(tel))*2*Math.PI/10000);
						if (startAng == null) startAng = Float.valueOf(aE);
						
						float deltaaE = (float) (8*Math.PI+aE-lastaE);
						deltaaE= (float) (deltaaE%(2*Math.PI));

						
						long currTime = System.currentTimeMillis();
						deltaTime = (int)(currTime-lastTime);
						//Log.d("m3denc",Double.toString(Math.toDegrees(deltaaE*1000/deltaTime)));
						measurment m = new measurment();
						m.speed = deltaaE*1000/deltaTime;
						m.aE = aE;
						m.time = currTime;
						measuments.add(m);
						if (measuments.size() > MAXQUE) measuments.poll();
						//if (startAng > Math.PI  && aE < Math.PI && aE > -Math.PI + startAng) done = true;
						//if (startAng < Math.PI  && aE > Math.PI && aE > Math.PI + startAng) done = true;
						//done = isMoreThanHalfRev(aE);
						lastTime = currTime;
					}
				}
				
				
			} catch (final IOException e2) {
				// TODO Auto-generated catch block
				
			}  catch (final NumberFormatException e)
			{
				Log.w("m3d", "problem with Perle");
			}
			catch (final InterruptedException e) {
				// TODO Auto-generated catch block
				
			}
			
		}
		myLaser.done = true;
		try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("m3d", "Perle thread ended");
	}
}