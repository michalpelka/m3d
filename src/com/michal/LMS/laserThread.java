package com.michal.LMS;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Deque;
import java.util.LinkedList;
import android.annotation.SuppressLint;
import android.util.Log;

@SuppressLint("NewApi")

public class laserThread implements Runnable {

	BufferedReader in;
	BufferedWriter out;
	public Deque<Scan> Scans = new LinkedList<Scan>();
	//private Scan LastScan;
	private String lastMsg = new String();
	private boolean msgWReady = false;
	private String ip = "192.168.1.202";
	private Socket s;
	boolean done = false;

	public boolean isDone()
	{
		return done;
	}
	public void setIp (String _ip)
	{
		ip =_ip;
	}
	public void assignEncoderMeasurment(float enc) {
		if (Scans.size()==0) return;
		Scan last = Scans.getLast();
		last.encPos =enc;
	}
	 private void addScan (Scan scan)
	{
		Scans.add(scan);
	}
	 private void dealWithMsg() {
		// szukamy DIST1
		Scan scan = new Scan();
		String[] myStrList = lastMsg.split(" ");
		int iDist1 = -1;
		
		for (int i = 0; i < myStrList.length; i++) {

			if (myStrList[i].compareTo(new String("DIST1")) == 0) {
				iDist1 = i;
				break;
			}
		}

		if (iDist1 > 0) {
			int i_scanNo = Integer.parseInt(myStrList[iDist1 + 5], 16);
			

			for (int i = 0; i < i_scanNo; i++) {
				scan.Dist1.add(Integer.parseInt(myStrList[iDist1 + 5 + i], 16));
			}
			addScan(scan);
			if (Scan.cos == null)
			{
				Scan.buildLookUpTable(scan.Dist1.size());
			}
		
		}
	}

	void sendMsg(String Msg) {
		Msg = ((char) 0x02) + Msg + (char) (0x03);
		try {
			out.write(Msg);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void readMsg() {

		if (!s.isConnected()) return;
		try {
			if (in.ready()) {
				char buf[] = new char[30000];

				int dataSize = in.read(buf);
				// Log.d("LMS", Integer.toString(dataSize));
				//Log.d("m3d", Integer.toString((int)buf[dataSize-1]));
			
					String temp = new String(buf, 0, dataSize);
					int beg = temp.lastIndexOf(0x02);
					int end = temp.lastIndexOf(0x03);
					
					//ca�y telegram w pakiecie
					if (beg>= 0 && end >= 0 && beg < end)
					{
						lastMsg = temp.substring(beg, end);
						msgWReady = true;
					}
					//�rodek telegramu w pakiecie
					if (beg==-1 && end ==-1)
					{
						msgWReady = false;
						lastMsg =lastMsg + temp;						
					}
					//koniec telegramu
					if (beg ==-1 && end >=0)
					{
						lastMsg =lastMsg + temp.substring(0, end);	
						msgWReady = true;
					}
					//tylko poczatek
					if (beg >=0 && end ==-1)
					{
						lastMsg = temp.substring(beg);
						msgWReady = false;
					}				
					if (msgWReady)dealWithMsg();
					msgWReady = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Log.d("m3d", "Laser thread started");
		boolean connected =false;
		done = false;
		Scans.clear();
		try {
			Log.d("LMS", "Staring");
			s = new Socket(ip, 2111);
			if(s.isConnected() ==false )
			{
				Log.w("LMS", "PROBLEM WITH TCP SOCKET ");
				return ;
			}
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new BufferedWriter(
					new OutputStreamWriter(s.getOutputStream()));

			Log.d("LMS", "Requesting scan");
			sendMsg("sEN LMDscandata 1");
			Log.d("LMS", "Listining...");
			connected = true;
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		int i=0;
		while (connected) {
			try {
				i++;
				if (i==10000)
				{
					Log.d("m3d", "Scan quey size:" + Integer.toString(Scans.size()));
					i=0;
				}
				if (s.getInputStream().available()>200)
				{
					readMsg();
				}
				else
				{
					if(done) break;
				}
				Thread.sleep(15);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("m3d", "Laser thread ended");
	}

}
