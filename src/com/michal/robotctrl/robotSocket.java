/*
 * robotSocket.java
 *
 * Created on July 26, 2007, 10:12 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
/*
 * robotSocket.java
 *
 * Modified for Android
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.michal.robotctrl;
import java.io.*;
import java.net.*;

import android.util.Log;
import android.widget.EditText;
/**
 *
 * @author Dr Robot Inc
 * @author Micha� Pe�ka 
 * PRAWIE NIEZMODYFIKOWANA KLASA OD KANADYJCZYK�W
 */
public class robotSocket implements Runnable {
	final String TAG="RobotCtrlTh";
    private DatagramSocket sock = null;
    private String robotip;
    private int robotport;
    private DatagramPacket recPacket = null;
    private DatagramPacket sendPacket = null;
    private InetAddress server = null;
    
    private byte []recBuffer;
    private byte []sendBuffer;
    private int []tempData;
    private byte []decodeBuffer;
    private int endIndex = 0;
    private ByteArrayInputStream bin = null;
    private DataInputStream din = null;
    private DataOutputStream out = null;
    private InputStream in = null;
    private BufferedReader reader = null;
    private String strCommand = "";
    public boolean runFlag = false;
    
    public int []EncoderPos = {0,0,0,0,0,0};
    
    //motor sensor 
   
    public int []EncoderSpeed = {0,0,0,0,0,0};
    public int []MotorPower = {0,0,0,0,0,0};
    public double []MotorTemp = {0.0,0.0,0.0,0.0,0.0,0.0};
    
    //custom sensor data
    public int []customAD = {0,0,0,0,0,0,0,0};
    public int customIO = 0;

    
    //standard sensor data
    
    public double BatteryVol = 0;
    
    //for temperature sensor
    final double[] resTable = {114660,84510,62927,47077,35563,27119,20860,16204,12683,10000,
                    7942,6327,5074,4103,3336,2724,2237,1846,1530,1275,1068,899.3,760.7,645.2,549.4};
    final double[] tempTable = { -20, -15, -10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 };
    final double FULLAD = 4095;
    
    /** Creates a new instance of robotSocket */
    public robotSocket(String robotIP, int robotPort) {
    
        
        this.robotip = robotIP;
        this.robotport = robotPort;
    
    
    }
    
    public void recon()
    {
    	
        //socket connect
        try {
	    
            sock = new DatagramSocket();
            sock.setSoTimeout(20);
	} catch(IOException e) {
		Log.e(TAG, "exception", e);
	}
        
        recBuffer = new byte[1024];
        recPacket = new DatagramPacket(recBuffer,recBuffer.length);
        
        decodeBuffer = new byte[2048];
        endIndex = 0;
        
        sendBuffer = new byte[256];
        sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length,server,robotport);
        
    }
    
   public void run(){
     
	   
	    try{
            server = InetAddress.getByName(robotip);
        }catch(IOException e) {
        	Log.e(TAG, "exception", e);
        	 
        }
                
        //socket connect
        try {
	    
            sock = new DatagramSocket();
            sock.setSoTimeout(20);
	} catch(IOException e) {
		Log.e(TAG, "exception", e);
	}
        
        recBuffer = new byte[1024];
        recPacket = new DatagramPacket(recBuffer,recBuffer.length);
        
        decodeBuffer = new byte[2048];
        endIndex = 0;
        
        sendBuffer = new byte[256];
        sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length,server,robotport);
        try{
            sock.send(sendPacket);
        }catch(IOException e){
            e.printStackTrace();
        }
        runFlag = true;
        
        Log.d(TAG, "Connected");
        
        
       while(runFlag)
       {
          // Log.d(TAG, "bbb");
                try{
                    sock.setSoTimeout(5);
                    sock.receive(recPacket);    
                }catch(IOException e){
          //          e.printStackTrace();
       
                }
                
                int Len = recPacket.getLength();
                if (Len != 0) {
                    //Log.d(TAG, "Msg from robot "  + Integer.toString(Len));
                    //first append the latest data to decode buffer
                    if (Len + endIndex < 2048){
                        for(int i = 0; i < Len; i++)
                        {
                            decodeBuffer[i + endIndex] = recBuffer[i];
                        }
                        endIndex = Len + endIndex;
                    }
                    else{
                        //discard old data, only keep the latest data
                        for (int i = 0; i < Len; i++)
                        {
                            decodeBuffer[i] = recBuffer[i];
                        }
                        endIndex = Len;
                            
                    }
                    //search package head
                    int startPos = -1;
                    int endPos = -1;
                    boolean keepsearch = true;
                    while(keepsearch)
                    {
                        
                        for (int i = 0; i < endIndex -1 ; i ++)
                        {
                            if ((decodeBuffer[i] == 94)&& (decodeBuffer[i + 1] == 2)){
                                //find the start 
                                startPos = i;
                                break;
                            }
                        }
                        if (startPos  < 0)  // not find start head of package, discard all data
                        {
                            keepsearch = false;
                            endIndex = 0;       // wait for next time receive data
                        }
                        else
                        {
                            //find the start head, search the end position
                            for (int i = startPos; i < endIndex - 1; i ++)
                            {
                                if ((decodeBuffer[i] == 94)&& (decodeBuffer[i + 1] == 13)){
                                    //find the start 
                                    endPos = i;
                                    break;
                                }
                            }
                            if (endPos - startPos > 5)
                            {
                                //find the end, copy it to tempData
                                 //decode here
                                int packageLen = endPos + 1 - startPos - 1;
                                tempData = new int[packageLen];
                                for (int i = 0; i < packageLen; i ++){
                                    tempData[i] = (int)(decodeBuffer[i + startPos] & 0xff);
                                }
                                //deal with this data package
                                DealWithPackage();
                                // remove the package from decodeBuffer, and search again
                                if(endPos + 2 < endIndex)
                                {
                                    for(int i = 0; i < endIndex - endPos - 1 ; i++)
                                    {
                                        decodeBuffer[i] = decodeBuffer[i + endPos + 2];
                                    }
                                    endIndex = endIndex - endPos - 1;
                                }
                                else
                                {
                                    keepsearch = false;
                                    endIndex = 0;
                                }
                            }
                            else
                            {
                                //wait for next package
                                keepsearch = false;
                            }
                                    
                        }
                       
                    }  //keepsearch
                    
                    
                }
                else
                {
                    try{
                        Thread.sleep(30);
                    }
                    catch(Exception e){
                        
                    }
                            
                }
                recPacket.setLength(recBuffer.length);
       }//run flag
       sock.close();
   }
   
   private void DealWithPackage(){
   int len = tempData.length;
        if (tempData[4] == 123){
            if (len > 37)
            {
                MotorPower[0] = tempData[5 + 1] +(tempData[5 + 2]) * 256;
                MotorPower[1] = tempData[5 + 3] +(tempData[5 + 4]) * 256;
                MotorPower[2] = tempData[5 + 5] +(tempData[5 + 6]) * 256;
                MotorPower[3] = tempData[5 + 7] +(tempData[5 + 8]) * 256;
                MotorPower[4] = tempData[5 + 9] +(tempData[5 + 10]) * 256;
                MotorPower[5] = tempData[5 + 11] +(tempData[5 + 12]) * 256;

                for(int i = 0; i < 6; i++)
                {
                    if (MotorPower[i] > 32767) MotorPower[i] = 16384;
                }

                EncoderPos[0] = tempData[5 + 25] +(tempData[5 + 26]) * 256;
                EncoderSpeed[0] = tempData[5 + 27] +(tempData[5 + 28]) * 256;
                EncoderPos[1] = tempData[5 + 29] +(tempData[5 + 30]) * 256;
                EncoderSpeed[1] = tempData[5 + 31] +(tempData[5 + 32]) * 256;
            }
        }
        else if(tempData[4] == 124){
            if (len > 33)
            {
                //custom sensor data
                for (int i = 0; i < 8; i ++){
                    customAD[i] = tempData[ 6 + 2*i] + tempData[6 + 2*i +1] * 256;    
                }
                // for Jaguar Lite, 
                //              [0] = left front motor, 
                //              [1] = right front motor, 
                // for Jaguar 4X4, 
                //              [0] = left front motor, 
                //              [1] = right front motor, 
                //              [2] = left rear motor, 
                //              [3] = right rear motor, 
                // for Jaguar V2
                //              [0] = left front motor, 
                //              [1] = fliper arm 1
                //              [2] = right front motor, 
                //              [3] = flipper arm 2
                MotorTemp[0] = AD2Temperature(customAD[4]); 
                MotorTemp[1] = AD2Temperature(customAD[5]); 
                MotorTemp[2] = AD2Temperature(customAD[6]); 
                MotorTemp[3] = AD2Temperature(customAD[7]); 

                //[4],[5] only for Jaguar V4, rear motor(left,right)
                MotorTemp[4] = AD2Temperature(customAD[3]); 
                MotorTemp[5] = AD2Temperature(customAD[4]); 


                EncoderPos[3] = tempData[5 + 18] +(tempData[5 + 19]) * 256;
                EncoderPos[4] = tempData[5 + 24] +(tempData[5 + 25]) * 256;
                EncoderSpeed[3] = tempData[5 + 20] +(tempData[5 + 21]) * 256;
                int dir = tempData[5 + 22];
                if (dir <= 0) 
                {
                    EncoderSpeed[3] = -EncoderSpeed[3];
                }
                EncoderSpeed[4] = tempData[5 + 26] +(tempData[5 + 27]) * 256;
                dir = tempData[5 + 28];
                if (dir <= 0) 
                {
                    EncoderSpeed[4] = -EncoderSpeed[4];
                }
                customIO = tempData[5 + 17];
            }
        }
        else if(tempData[4] == 125){
            if (len > 38)
            {
                //standard sensor
                BatteryVol = (double)(tempData[5 + 33] + tempData[5 + 34] * 256)/ 4095.0 * 34.498;
            }
        }
                        
}

   
     public void sendCommand(byte []CmdArray){
          for (int i = 0; i< CmdArray.length ; i ++){
              sendBuffer[i] = CmdArray[i];
          }
         sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length,server,robotport);
        try{
            sock.send(sendPacket);
        }catch(IOException e){
            e.printStackTrace();
        }
      }
   
     private double AD2Temperature(int adValue){
         double tempM = 0;
            double k = (adValue / FULLAD);
            double resValue = 0;
            if (k != 1)
            {
                resValue = 10000 * k / (1 - k);      //AD value to resistor
            }
            else
            {
                resValue = resTable[0];
            }


            int index = -1;
            if (resValue >= resTable[0])       //too lower
            {
                tempM = -20;
            }
            else if (resValue <= resTable[24])
            {
                tempM = 100;
            }
            else
            {
                for (int i = 0; i < 24; i++)
                {
                    if ((resValue <= resTable[i]) && (resValue >= resTable[i + 1]))
                    {
                        index = i;
                        break;
                    }
                }
                if (index >= 0)
                {
                    tempM = tempTable[index] + (resValue - resTable[index]) / (resTable[index + 1] - resTable[index]) * (tempTable[index + 1] - tempTable[index]);
                }
                else
                {
                    tempM = 0;
                }

            }

            return tempM;
      }
      

   
}
