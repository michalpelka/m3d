package com.michal.robotctrl;

import com.michal.util.MyApp;

import android.util.Log;
import android.widget.TextView;

public class RobotThread implements Runnable {

	private Thread worker;
	private robotSocket sock = null;
	private String robotIP = "";
	private int robotPort = 0;

	private int lEnc = 0;
	private int rEnc = 0;
	private float voltage =0;
	private boolean isLightOn = false;
	public boolean isPaused = false;
	final String TAG = "RobotCtr";
	private boolean ConnectFlag = false;
	private boolean done = false;
	final byte STX0 = 94;
	final byte STX1 = 2;
	final byte ETX0 = 94;
	final byte ETX1 = 13;

	final byte PWMMOVE = 6; // referee protocol, open loop PWM control
	final byte SPEEDMOVE = 27; // referee protocol, velocity control

	final byte PINGCMD = (byte) 255;
	final byte DCMOTORPARAMETERSET = 7; //

	final byte DCMOTORCTRLMODE = 14; //

	final byte DCPOSITIONPID = 7; // positon PID Control
	final byte DCVELOCITYPID = 8; // velocity PID Control

	final byte PWMCTRL = 0;
	final byte POSITIONCTRL = 1;
	final byte VELOCITYCTRL = 2;

	final byte KpID = 1;
	final byte KdID = 2;
	final byte KiID = 3;

	final byte CUSTOMGPIODOUT = 22; // referee protocol, set custom GPIO out
	final byte DCMOTORPOSITION = 3; // referee protocol, specified DC motor
									// Position control
	final byte ALLDCMOTORPOSITION = 4; // all DC motor positon control
	final byte DCMOTORPWM = 5; // referee protocol, specified DC motor PWM
								// control
	final byte ALLDCMOTORPWM = 6; // all DC motor PWM control
	final byte DCMOTORVELOCITY = 26; // referee protocol,specified DC motor
										// Velocity control
	final byte ALLDCMOTORVELOCITY = 27; // all DC motor positon control

	final int NONCTRLCMD = 0xffff; // no ctrl command
	final int INIPWM = 16384;
	// some robot parameters, depend on what kind of robot you rae using, here
	// is for X80, if you have some questions,please send e-mail to
	// info@drrobot.com
	final double WheelDis = 0.265; // wheel distance
	final double WheelR = 0.0825; // wheel radius
	final int CircleCnt = 190; // encoder one circle count
	private int pwmPower1 = INIPWM;
	private int pwmPower2 = INIPWM;
	private int speed = 5000;

	public synchronized void setSpeed(float linear, float anglurar)

	{
	
		if (!isPaused)
		{
			pwmPower1 = INIPWM + (int) (linear * speed * 2);
			pwmPower2 = INIPWM + (int) (anglurar * speed * 3);
		}else
		{
			pwmPower1 =INIPWM;
			pwmPower2 =INIPWM;
			
		}
	}

	private byte calc_CRC_bit(byte[] CmdArray) {
		byte shift_reg, sr_lsb, data_bit, v;
		int i, j;
		byte fb_bit;
		shift_reg = 0; // initialize the shift register
		for (i = 0; i < CmdArray.length - 5; i++) {
			v = (byte) (CmdArray[2 + i]); // start from RID
			for (j = 0; j < 8; j++) { // for each bit
				data_bit = (byte) ((v & 0x01) & 0xff); // isolate least sign bit
				sr_lsb = (byte) ((shift_reg & 0x01) & 0xff);
				fb_bit = (byte) (((data_bit ^ sr_lsb) & 0x01) & 0xff); // calculate
																		// the
																		// feed
																		// back
																		// bit
				shift_reg = (byte) ((shift_reg & 0xff) >>> 1);
				if (fb_bit == 1) {
					shift_reg = (byte) ((shift_reg ^ 0x8c) & 0xff);
				}
				v = (byte) ((v & 0xff) >>> 1);
			}
		}
		return shift_reg;
	}

	private void SendPingCmd() {
		byte[] msg = new byte[10];

		msg[0] = STX0;
		msg[1] = STX1;
		msg[2] = 1;
		msg[3] = 0;
		// DID
		msg[4] = PINGCMD;

		msg[5] = 1;// Length;

		msg[6] = 0;

		msg[7] = calc_CRC_bit(msg);// Checksum

		msg[8] = ETX0;
		msg[9] = ETX1;

		sock.sendCommand(msg);
	}

	private void SetCustomDOUT(int dout) {
		byte[] msg = new byte[10];

		msg[0] = STX0;
		msg[1] = STX1;
		msg[2] = 1;
		msg[3] = 0;
		// DID
		msg[4] = CUSTOMGPIODOUT;

		msg[5] = 1;// Length;

		msg[6] = (byte) (dout & 0xff);

		msg[7] = calc_CRC_bit(msg);// Checksum

		msg[8] = ETX0;
		msg[9] = ETX1;

		sock.sendCommand(msg);

	}

	private void PwmNonTimeCtrlAll(int cmd1, int cmd2, int cmd3, int cmd4,
			int cmd5, int cmd6) {
		byte[] DCMotorSpeedCmd;
		DCMotorSpeedCmd = new byte[21];
		DCMotorSpeedCmd[0] = STX0;
		DCMotorSpeedCmd[1] = STX1;
		DCMotorSpeedCmd[2] = 1;
		DCMotorSpeedCmd[3] = 0;
		DCMotorSpeedCmd[4] = ALLDCMOTORPWM; // DID
		DCMotorSpeedCmd[5] = 12; // length
		DCMotorSpeedCmd[6] = (byte) (cmd1 & 0xff); // motor 1
		DCMotorSpeedCmd[7] = (byte) ((cmd1 >>> 8) & 0xff);
		DCMotorSpeedCmd[8] = (byte) (cmd2 & 0xff); // motor 2
		DCMotorSpeedCmd[9] = (byte) ((cmd2 >>> 8) & 0xff);
		DCMotorSpeedCmd[10] = (byte) (cmd3 & 0xff); // motor 3
		DCMotorSpeedCmd[11] = (byte) ((cmd3 >>> 8) & 0xff);
		DCMotorSpeedCmd[12] = (byte) (cmd4 & 0xff); // motor 4
		DCMotorSpeedCmd[13] = (byte) ((cmd4 >>> 8) & 0xff);
		DCMotorSpeedCmd[14] = (byte) (cmd5 & 0xff); // motor 5
		DCMotorSpeedCmd[15] = (byte) ((cmd5 >>> 8) & 0xff);
		DCMotorSpeedCmd[16] = (byte) (cmd6 & 0xff); // motor 6
		DCMotorSpeedCmd[17] = (byte) ((cmd6 >>> 8) & 0xff);
		DCMotorSpeedCmd[18] = calc_CRC_bit(DCMotorSpeedCmd); // check sum
		DCMotorSpeedCmd[19] = ETX0;
		DCMotorSpeedCmd[20] = ETX1;
		sock.sendCommand(DCMotorSpeedCmd);
	}

	private robotSocket mrSocket;

	public void setRobotIPPort(String IP, int port) {
		this.robotIP = IP;
		this.robotPort = port;
	}

	public void resume()
	{
		if (sock != null){
			sock.recon();
		}
		
	}
	public void setDone()
	{
		done =true;
	}
	@Override
	public void run() {

		sock = new robotSocket(robotIP, robotPort);

		Thread worker = new Thread(sock);

		worker.start();

		while (!done) {

			try {

				Thread.sleep(100);
				SendPingCmd();
				PwmNonTimeCtrlAll(NONCTRLCMD, NONCTRLCMD, NONCTRLCMD,
						pwmPower1, pwmPower2, NONCTRLCMD);
				  voltage=(float) sock.BatteryVol;
			      rEnc=(sock.EncoderPos[0]);
			      lEnc=(sock.EncoderPos[1]);
			      //Log.d("RobotCNTRL", Integer.toString(rEnc)+", "+Integer.toString(lEnc));
			      MyApp.getInstance().ReportOdo(rEnc, lEnc);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		PwmNonTimeCtrlAll(NONCTRLCMD, NONCTRLCMD, NONCTRLCMD,
				INIPWM, INIPWM, NONCTRLCMD);
		PwmNonTimeCtrlAll(NONCTRLCMD, NONCTRLCMD, NONCTRLCMD,
				INIPWM, INIPWM, NONCTRLCMD);
		PwmNonTimeCtrlAll(NONCTRLCMD, NONCTRLCMD, NONCTRLCMD,
				INIPWM, INIPWM, NONCTRLCMD);
		sock.runFlag= false;
		try {
			worker.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("RobotCTRL", "robot socket done");
	
	}

	float getVoltage()
	{
		return voltage;
	}
	int getLEnc()
	{
		return lEnc;
	}
	int getREnc()
	{
		return rEnc;
	}
	
	public void setLight(boolean on) {

	}
}
