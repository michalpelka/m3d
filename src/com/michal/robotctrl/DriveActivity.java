package com.michal.robotctrl;

import java.util.Timer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.InputDevice;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.michal.m3d.R;
import com.michal.util.Config;
import com.michal.util.MyApp;

@SuppressLint("NewApi")
public class DriveActivity extends Activity {

	
	private SeekBar seekBar1;
	private SeekBar seekBar2;
	private InputDevice mLastInputDevice;
	private TextView tVBattery;
	private TextView tVEnc1;
	private TextView tVEnc2;
	private static RobotThread rt;
	private VideoView v;
	//private static Thread handler_rt;
	private int MagX;
	private int MagY;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drive);
		seekBar1 = (SeekBar) findViewById(R.id.seekBar1);
		seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
		tVBattery = (TextView) findViewById(R.id.textViewBattery);
		tVEnc1 = (TextView) findViewById(R.id.textEnc1);
		tVEnc2 = (TextView) findViewById(R.id.textEnc2);
		rt = MyApp.getInstance().getRobotThread();
		v = (VideoView) findViewById(R.id.videoView1);
		//MediaController mcont = new MediaController(this);
		//rtsp://<ip>/axis-media/media.3gp
		//Uri uri = Uri.parse("rtsp://media.smart-streaming.com/mytest/mp4:sample.mp4");
		Uri uri = Uri.parse(Config.URicamera);
		v.setMediaController(null);
		v.setVideoURI(uri);
		
		//v.requestFocus();
		v.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.drive, menu);
		return true;
	}
	
	
	
	
	public void onPause()
	{
	  super.onPause();
	  //rt.setDone();
	  rt.isPaused= true;
	  rt.setSpeed(0,0);
	}
	public void onResume()
	{
	  super.onResume();
	  //rt.setDone();
	  rt.isPaused= false;
	  rt.setSpeed(0,0);
	  rt.resume();
	  
	}
	@SuppressWarnings("unused")
	public boolean onGenericMotionEvent(MotionEvent event)
	 {
		

       // Check that the event came from a joystick since a generic motion event
       // could be almost anything.
       if ((event.getSource() & InputDevice.SOURCE_CLASS_JOYSTICK) != 0
               && event.getAction() == MotionEvent.ACTION_MOVE) {
           // Cache the most recently obtained device information.
           // The device information may change over time but it can be
           // somewhat expensive to query.
           if (mLastInputDevice == null || mLastInputDevice.getId() != event.getDeviceId()) {
               mLastInputDevice = event.getDevice();
               // It's possible for the device id to be invalid.
               // In that case, getDevice() will return null.
               if (mLastInputDevice == null) {
                   return false;
               }
           }

           // Ignore joystick while the DPad is pressed to avoid conflicting motions.
        
           // Process all historical movement samples in the batch.
         
          float X = event.getX();
          float Y = event.getY();
          float Z = event.getAxisValue(MotionEvent.AXIS_Z);
          float Z2 =  event.getAxisValue(MotionEvent.AXIS_RZ);
          int max = seekBar1.getMax();
          float x = (float) (0.5*X*max+0.5*max);
          float y = (float) (0.5*Y*max+0.5*max);
          float z = (float) (0.5*Z*max+0.5*max);
          float z2 = (float) (0.5*Z2*max+0.5*max);
          
          seekBar1.setProgress((int)(y));
          seekBar2.setProgress((int)(z));
          rt.setSpeed(Y, Z);
          MyApp.getInstance().ReportJoy(Y, Z);
          int _MagX = MyApp.getInstance().getIMUThread().accX;
          int _MagY = MyApp.getInstance().getIMUThread().accY;
          if (_MagX != 0)
          {
        	  MagX = _MagX;
        	  MagY = _MagY;
          }
          
          tVBattery.setText("MagX:"+Integer.toString(MagX)+"\nMagY:"+Integer.toString(MagY)+String.format("\n %.1f [V]", rt.getVoltage()));
	      tVEnc1.setText(Integer.toString(rt.getLEnc()));
	      tVEnc2.setText(Integer.toString(rt.getREnc()));
	      
           return true;
       }
       return false;
	 }
	 
	 
//	private int elapsedTime;
//	private Timer timer;
	


}
