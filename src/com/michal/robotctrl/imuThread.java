package com.michal.robotctrl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import android.util.Log;

import com.michal.util.Config;
//import com.michal.util.MyApp;
import com.michal.util.MyApp;

public class imuThread implements Runnable {

	public int accX, accY, accZ, gyrX, gyrY, gyrZ, magX,magY,magZ;
	
	private Socket s;
	BufferedReader in;
	private boolean done = false;

	
	
	public void setDone ()
	{
		done = true;
	}
	void dealWithLine(String line)
	{
		if (line.length()< 26) return;
		int ind;
		try{
		line = line.substring(1,line.length()-1);
		String n[] = line.split(",");
		if (n.length <10 )return;
		ind = Integer.parseInt(n[0]);
		accX = Integer.parseInt(n[1]);
		accY = Integer.parseInt(n[2]);
		accZ = Integer.parseInt(n[3]);
		gyrX = Integer.parseInt(n[4]);
		gyrY = Integer.parseInt(n[5]);
		gyrZ = Integer.parseInt(n[6]);
		magX = Integer.parseInt(n[7]);
		magY = Integer.parseInt(n[8]);
		magZ = Integer.parseInt(n[9]);
		MyApp.getInstance().ReportIMU(ind,accX, accY, accZ, gyrX, gyrY, gyrZ, magX, magY, magZ);
		//Log.d("m3d","X:"+Integer.toString(accX)+"\tY:"+Integer.toString(accY)+"\tZ:"+Integer.toString(accZ));
		//Log.d("m3d", Integer.toString(ind));
		}
		catch(Exception e)
		{
			Log.w("m3d", "IMU COMM PROBLEM");
		
		}
	}
	@Override
	public void run() {
		
		
		try {
			s = new Socket(Config.IPImu, 10001);
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			s.setReceiveBufferSize(30);
			while (!done)
			{
				Thread.sleep(0);
				if (s.getInputStream().available()>24)
				{
					Thread.sleep(2);
					String l = in.readLine();
					dealWithLine(l);
				}
			}
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
}
