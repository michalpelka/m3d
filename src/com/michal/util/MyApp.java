package com.michal.util;




import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;



import android.R.array;
import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.sax.StartElementListener;
import android.util.Log;



import com.michal.robotctrl.RobotThread;
import com.michal.robotctrl.imuThread;




class DatabaseHelper extends SQLiteOpenHelper
{
	 // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
 
    public DatabaseHelper(Context context) {
        super(context, com.michal.util.Config.DatabaseFileDir, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE points(key INTEGER PRIMARY KEY, sessionID INTEGER, X Real, Y Real, Z Real);";
        db.execSQL(CREATE_CONTACTS_TABLE);
        String CREATE_SESSION_TABLE = "CREATE TABLE sessions(sessionID INTEGER PRIMARY KEY, TimeStamp INTEGER ,time DATETIME DEFAULT CURRENT_TIMESTAMP, some INTEGER);";
        db.execSQL(CREATE_SESSION_TABLE);
        String CREATE_IMU_TABLE = "CREATE TABLE imu(sessionID INTEGER, "+
        "ind INTEGE, AccX Real, AccY Real, AccZ Real, GyrX Real, GyrY Real,GyrZ Real, MagX Real, MagY Real, MagZ Real, TimeStamp INTEGER);";
        db.execSQL(CREATE_IMU_TABLE);
        String CREATE_ODO_TABLE = "CREATE TABLE odometry(sessionID INTEGER, "+
                " odo1 INTEGER,odo2 INTEGER, TimeStamp INTEGER);";
        db.execSQL(CREATE_ODO_TABLE);
        String CREATE_JOY_TABLE = "CREATE TABLE joy(sessionID INTEGER ,"+
                " X Real, Y Real, TimeStamp INTEGER);";
        db.execSQL(CREATE_JOY_TABLE);
        
        
    }
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

}

class dbInserter
{
	private class r implements Runnable
	{
		public void run() {
			
			load();
		}
	}
	private void load()
	{
		Log.d("m3d", "data load started");
		SQLiteDatabase db = MyApp.getInstance().getDatabase();
		db.beginTransaction();
		for (int i =0; i < cvalues.size(); i++)
		{
			db.insert(ctables.get(i), null, cvalues.get(i));		
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		ctables.clear();
		cvalues.clear();
		Log.d("m3d", "data load ended");
	}
	synchronized void copy()
	{
		ctables.addAll(tables);
		cvalues.addAll(values);
		values.clear();
		tables.clear();
	}
	private ArrayList<String> tables = new ArrayList<String>();
	private ArrayList<ContentValues> values = new ArrayList<ContentValues>();
	private ArrayList<String> ctables = new ArrayList<String>();
	private ArrayList<ContentValues> cvalues = new ArrayList<ContentValues>();
	synchronized void addToDb(String table, ContentValues v)
	{
		tables.add(table);
		values.add(v);
		if (values.size()>200)
			{
				copy();
				new Thread(new r()).start();
			}
	}
	
}

public class MyApp extends Application{
	
	dbInserter dbI = new dbInserter();
	private static MyApp sInstance;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	private long LastSessionID =-1;
    @Override
    public void onCreate() {
      super.onCreate();  

      Log.d("m3d", "Singleton instance created");
      sInstance = this;
      dbHelper = new DatabaseHelper(this.getApplicationContext());
      db = dbHelper.getWritableDatabase();
    }
    
   
    public static  MyApp getInstance()
    {
    	return sInstance;
    }
    	

    public long RegisterSession()
    {
    	SQLiteDatabase db = MyApp.getInstance().getDatabase();
		//Log.d("m3d", "SQL Transaction BEGIN");
		
		ContentValues values = new ContentValues();
		values.put("some", "0");
		values.put("TimeStamp", System.currentTimeMillis());
		LastSessionID = db.insert("sessions", null, values);
		Log.d("m3d", "Registered Session ID: "+Long.toString(LastSessionID));
		return LastSessionID;
    }
    public void ReportJoy (Float x, Float y)
    {
    	if(!Config.recordMission) return;
    	ContentValues values = new ContentValues();
    	values.put("sessionID", LastSessionID);
    	values.put("TimeStamp", System.currentTimeMillis());
    	values.put("X", x);
    	values.put("Y", y);	
    	//db.insert("joy", null, values);
    	dbI.addToDb("joy", values);
    }
    public void ReportOdo (int odo1, int odo2)
    {
    	if(!Config.recordMission) return;
    	ContentValues values = new ContentValues();
    	values.put("sessionID", LastSessionID);
    	values.put("TimeStamp", System.currentTimeMillis());
    	values.put("odo1", odo1);
    	values.put("odo2", odo2);	
    	//db.insert("odometry", null, values);
    	dbI.addToDb("odometry", values);
    }
    public void ReportIMU (int ind, float accX ,float accY, float accZ, float gyrX, float gyrY, float gyrZ, float magX, float magY, float magZ)
    {
    	if(!Config.recordMission) return;
    	ContentValues values = new ContentValues();
    	values.put("sessionID", LastSessionID);
    	values.put("TimeStamp", System.currentTimeMillis());
    	values.put("ind", ind);
    	values.put("AccX", accX);
    	values.put("AccY", accY);
    	values.put("AccZ", accZ);
        
    	values.put("GyrX", gyrX);
     	values.put("GyrY", gyrY);
     	values.put("GyrZ", gyrZ);
        
     	values.put("MagX", magX);
     	values.put("MagY", magY);
     	values.put("MagZ", magZ);
        
    	//db.insert("IMU", null, values);
     	dbI.addToDb("IMU", values);
    	
    }
    
	public int connectToRobot()
	{
		connectToRobot(Config.IProbot,Config.robotPort);
		
		return 1;
	}
	public void disconnetAndJoinThreads()
	{
		rt.setDone();
		it.setDone();
		
		
		try {
			handler_it.join();
			handler_rt.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int connectToRobot(String robotIP, int robotPort )
	{
		Log.d("RbtCtrl", "connecting to " + robotIP);
		
		rt.setRobotIPPort(robotIP, robotPort);
		if (handler_rt == null)
		{
			
			handler_rt = new Thread(rt);
			handler_rt.start();
		}
		if (handler_it == null)
		{
			handler_it = new Thread(it);
			handler_it.start();
		}
		
		
		return 1;
	}
	public SQLiteDatabase getDatabase()
	{
		return db;
	}
	public RobotThread getRobotThread()
	{
		return rt;
	}
	public imuThread getIMUThread()
	{
		return it;
	}
	private static imuThread it = new imuThread();
	private static Thread handler_it;
	private static RobotThread rt = new RobotThread();
	private static Thread handler_rt;
	
}
