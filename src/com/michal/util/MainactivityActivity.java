package com.michal.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import com.michal.m3d.R;
import com.michal.m3d.R.id;
import com.michal.m3d.R.layout;
import com.michal.m3d.R.menu;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


class checker implements Runnable
{
	boolean isRotatingLaser =false;
	boolean isFrontLaser=false;
	boolean isEncoder=false;
	boolean isRobot=false;
	final Handler mHandler = new Handler();
	TextView tv;
	public checker(TextView tv) {
		this.tv = tv;
	} 
	@Override
	public void run() {
		try {
			InetAddress adr = InetAddress.getByName(Config.IProbot);
			isRobot = adr.isReachable(1000);
			adr = InetAddress.getByName(Config.IProtatingLaser);
			isRotatingLaser = adr.isReachable(1000);
			adr = InetAddress.getByName(Config.IPfrontLaser);
			isFrontLaser = adr.isReachable(1000);
			adr = InetAddress.getByName(Config.IPencoder);
			isEncoder = adr.isReachable(1000);
			mHandler.post(new Runnable() {
				
				@Override
				public void run() {
					tv.setText(tv.getText() + "\n isRobot: "
							+ Boolean.toString(isRobot));
					tv.setText(tv.getText() + "\n isFrontLaser: "
							+ Boolean.toString(isFrontLaser));
					tv.setText(tv.getText() + "\n isRotLaser: "
							+ Boolean.toString(isRotatingLaser));
					tv.setText(tv.getText() + "\n isPerle: "
							+ Boolean.toString(isEncoder));
					
				}
			});
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
public class MainactivityActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainactivity);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mainactivity, menu);
		return true;
	}
	public void onDriveMode (View w)
	{
		Intent myIntent = new Intent(MainactivityActivity.this, com.michal.robotctrl.DriveActivity.class);
		MainactivityActivity.this.startActivity(myIntent);
	}
	public void onScanMode (View w)
	{
		Intent myIntent = new Intent(MainactivityActivity.this, com.michal.m3d.OpenGLES20Activity.class);
		MainactivityActivity.this.startActivity(myIntent);
	}
	public void onDisconnect (View w)
	{
		
	}
	
	
	public void onConnect(View w)
	{
		Toast.makeText(getApplicationContext(), "Connecting to robot",
			Toast.LENGTH_LONG).show();
		MyApp.getInstance().connectToRobot();
	}

	public void onCheck(View w) {

		
		final boolean isRotatingLaser =false;
		final boolean isFrontLaser=false;
		final boolean isEncoder=false;
		final boolean isRobot=false;
		
		

		Toast.makeText(getApplicationContext(), "Checking accesible resources - wait",
				Toast.LENGTH_LONG).show();
		final TextView tv = (TextView) findViewById(R.id.textView1);
		tv.setText("");
		tv.setText(tv.getText() + "Checking configuration: network connections");
		new Thread (new checker(tv)).start();
	

			
	
			
			
		
	};

	// Robot controler

}
