package com.michal.util;

public class Config {
	public static final int SAVE_MODE_BINARY=1;
	public static final int SAVE_MODE_TXT=2;
	public static final int SAVE_MODE_SQLLITE=3;
	public static final int SAVE_MODE_NOSAVE=4;
	
	public static String IProtatingLaser = "192.168.0.200";
	public static String IPfrontLaser = "192.168.0.202";
	public static String IPencoder = "192.168.0.199";
	public static String IProbot = "192.168.0.60";
	public static String IPImu = "192.168.0.61";
	
	public static int robotPort = 10001;
	public static String URicamera = "rtsp://root:drrobot@192.168.0.65/axis-media/media.amp?videocodec=h264&audio=0&streamprofile=low";
	public static int encoderTicks = 5000; 
	public static boolean recordMission = true; 
	//public static float od = 65;
	public static float od = 0.1f;
	//shield
	public static String DatabaseFileDir = "/storage/sdcard1/measurment.db";
	public static String DatabaseDir = "/storage/sdcard1/";
	
	public static int saveMode = SAVE_MODE_BINARY;
	//tablet
	//public static String DatabaseFileDir = "/mnt/sdcard/measurment.db";
	
	public static boolean allowDbLoadDuringScan = false;		
	public float masteScale =0.001f; //meters
	
	public static int GLESVertexBufferSize = 4000000; // 4 000 000 ;
}
